METAPACKAGES = ['wireless', 'web', 'sdr',
                'voip', 'top10', 'rfid',
                'pwtools', 'gpu', 'forensic']

def get_mpkg(mp):
    out = os.system('apt-get kali-linux-{0} | grep Depends'.format(mp))
    if out.startswith('Depends:'):
        out = out.replace('Depends:', '')
        return [o.strip(', ') for o in out.split()]
    return None

class Menu(object):

    options = {
        'q': sys.exit
    }

    mode = 'select'

    def __init__(self, pkgs, back):
        for p in pkgs:
            self.options[str(pkgs.index(p))] = p
        self.options.update({
            'a': self.install_all,
            'm': self.toggle_mode,
            'b': back
        })

    def
